import { createGlobalStyle } from 'styled-components'

export const Styles = createGlobalStyle`
    * {
        box-sizing: border-box;
        margin: 0;
        padding: 0;
        outline: none;
    }

    html,
    body {
        margin: 0;
        padding: 0;
        font-size: 12px;
    }
`
