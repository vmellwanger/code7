import axios, { AxiosInstance } from "axios";

let jsonPlaceholderApi: AxiosInstance;

const instanceApi = () => {
    const instancia = axios.create({
        baseURL: "https://provadev.xlab.digital/api/v1",
        responseType: "json",
    });
    return instancia;
};

jsonPlaceholderApi = instanceApi();

export { jsonPlaceholderApi };
