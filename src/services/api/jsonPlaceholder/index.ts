import axios, { AxiosInstance } from "axios";

let jsonPlaceholderApi: AxiosInstance;

const instanceApi = () => {
    const instancia = axios.create({
        baseURL: "https://jsonplaceholder.typicode.com",
        responseType: "json",
    });
    return instancia;
};

jsonPlaceholderApi = instanceApi();

export { jsonPlaceholderApi };
